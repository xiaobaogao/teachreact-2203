// 使用css in js   步骤:

//01: 引入对应依赖模块 

import styled from 'styled-components'
//02: 定义组件名, 以前css里怎么写样式,现在就在模板字符串中怎么写样式
// 原理: 使用h1标签包裹 对应的样式代码,同时会给H1标签添加一个随机的类名,该类名上设置样式,避免样式冲突
export const Title = styled.h1`
  background:green;
  font-size:30px
`

// 样式的继承
export const Title2=styled(Title)`
  border:5px solid red
`

// 接受参数
// 函数必须有返回值,函数的形参props 就是接受的参数对象
export const Title3=styled.div`
   color:${(props)=>{return props.color}};
   font-size:20px
`



