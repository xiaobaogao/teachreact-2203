
// 该代理配置主要解决跨域问题
const { createProxyMiddleware } = require('http-proxy-middleware');
//  当访问的地址中包含 /api 字段,然后代理到 target 地址,.
// 因为访问本地 /api 请求,这是前端想本地服务器发起的请求,本地服务器将请求代理到远程服务器地址target,
// 注意: 跨域是因为浏览器的同源策略限制

module.exports = function(app) {
    app.use(
        createProxyMiddleware('/api', { 
            target: 'https://api.i-lynn.cn/college',
            changeOrigin: true,
            pathRewrite: { '^/api': '' }
        })
    )
}
