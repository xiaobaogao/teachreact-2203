
import { Component } from 'react'
// 高阶组件:
// 本质: 定义高阶函数, 用来接收一个组件,然后通过该函数,将其处理成一个新的组件返回
// 注意: 高阶函数可以返回函数组件,也可以返回类组件
// 注意: 当被包裹的组件有接收父组件传递的属性,由于被hoc中间组件包裹,所以属性属性无法接收到,
// 可以使用 {...this.props} 给hoc中组件添加,这样就实现了过渡.

function HOC(Com) {
    return class NewCom extends Component {
        render() {
            console.log(this.props);
            return (
                <div style={{ background: 'red', fontSize: '20px' }}>
                    <Com {...this.props}></Com>
                    <div>&copy; 2020 千峰教育</div>
                </div>
            )
        }
    }
}

export default HOC