import React, { Component } from 'react';
// 引入 Route 组件坑
import { Route, Redirect, Switch,withRouter } from 'react-router-dom'
// 引入对应的组件页面
import Home from '../views/router-views/Home';
import Category from '../views/router-views/Category'
import Car from '../views/router-views/Car'
import Mine from '../views/router-views/Mine'
import Notfind from '../views/router-views/Notfind';

//  路由组件的三种渲染方式
// 01: 第一种方式:
//  component = {组件名}  最常用
//  component = {(props)=>{return <组件名 {...props}></组件名>}}

//02:第二种方式
// render方式只有函数形式
// render ={(props)=>{return <组件名 {...props}></组件名>}
// 默认没有props 上个3个路由参数

//03:第三种方式
// children 方式
//  children={<组件名></组件名>}   没有props 需要使用WithRouter 包裹当前的组件
//  children ={(props)=>{return <组件名 {...props}></组件名>}





class Index extends Component {
    render() {
        // console.log('Index',this);
        return (
            <>
                <Switch>
                    <Route path='/home' component={Home}></Route>
                    {/* <Route path='/home' component={(props)=>{return <Home {...props}></Home>}}></Route> */}
                    <Route path='/category/:id?' component={Category}></Route>
                    {/* <Route path='/category/:id?' render={(props)=>{return <Category {...props}/>}}></Route> */}
                    {/* <Route path='/car' component={Car}></Route> */}
                    {/* <Route path='/car' children={<Car {...this.props}></Car>}></Route> */}
                    <Route path='/car' children={(props)=><Car {...props}></Car>}></Route>
                    <Route path='/mine' component={Mine}></Route>
                    {/* 
                        设置首页的重定向 重定向到home ,
                        from 表示当访问哪一个路径的时候重定向,to 重定向到哪一个路径
                        exact 属性表示严格模式,写在路由规则的后边  
                    */}
                    <Redirect from='/' to='/home' exact></Redirect>
                    {/* 
                       使用Route组件不设置path 属性这样表示任何路径都能匹配该组件,Switch 表示只能匹配一个
                       路由规则, 注意:一般 404规则写在当前路由规则的最后方
                      */}
                    <Route component={Notfind}></Route>
                </Switch>
            </>
        );
    }
}

export default withRouter(Index);
