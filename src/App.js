
import './App.css';

import React, { Component } from 'react';

import { Link, Route, withRouter } from 'react-router-dom';
// 引入自定义的导航组件 Mynav
import Mynav from './views/mynav/Mynav'
 import Rule from './router/index'
class App extends Component {
  render() {
    console.log('render',this);
    const arr = ['/home','/category','/car','/mine']
    return (
      <div>
        {/* 定义导航部分 类比vue中的 <router-link to>*/}
        <p>
          {/* 查询字符串 */}
          {/* <Link to='/home?id=100' tag='span'>首页</Link> */}
          {/* 第二种 使用restful 风格 */}
          {/* <Link to={'/category/' + 99}>分类</Link> */}
          {/* 第三种方式 隐式传参方式  常用语页面埋点,一般用于统计用户信息,收集方便分析产品的用户需求*/}
          {/* <Link to={{ pathname: '/car', search: 'id=1&id2=2', state: { a: 1, b: 2 } }}>购物车</Link> */}
          {/* <Link to='/mine'>我的</Link> */}
        </p>
        {/* 自定义导航组件 */}
        {/* 编程式导航 */}
        {/* <p>
          <span onClick={() => this.go({ pathname: '/home', search: 'id=1&id2=2', state: { a: 1, b: 2 } })}>首页</span>
          <span onClick={() => this.go('/category')}>分类</span>
          <span onClick={() => this.go('/car')}>购物车</span>
          <span onClick={() => this.go('/mine')}>我的</span>
        </p> */}

        {/* 定义的路由对应显示的组件 类比vue中的 <router-view> */}
        <Rule></Rule>
      </div>
    );
  };
  go(url) {
    // 编程式导航通过 :this.props.history.push(url)
    // 注意: 并不是所有的组件的props 属性都要路由的三个参数的 history, loaction match,当该组件是通过路由跳转过来的,那么该组件
    // 就有这三个参数,否则就没有 ,这时候就可以使用react-router-dom中提供的高阶组件强化函数 withRouter(组件) 被包裹组件就有三个参数了

    // console.log(this);
    this.props.history.push(url)
  }
}

export default withRouter(App);
