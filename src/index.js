
// 引入react.js 
import React from 'react';
// 引入react-dom.js
import ReactDOM from 'react-dom/client';
// 引入全局的css文件
import './index.css';
// 引入根组件
import App from './views/hooks/myhooks/Myhook';
// 谷歌新增的性能优化库文件(国内一般使用不了,因为服务器在国外)
// import reportWebVitals from './reportWebVitals';

// 引入 provider组件
import { Provider } from 'react-redux'
// 引入 store 
import store from './store'
import { BrowserRouter as Router } from 'react-router-dom'
// 两种路由模式

// 创建根实例 相当于 new Vue()
const root = ReactDOM.createRoot(document.getElementById('root'));

// 渲染组件或标签元素
// React.StrictMode 使用严格模式,严格模式的作用就是用来做一些 比如有些废弃的语法(表示不再维护更新),如果使用的话,在严格模式下会有控制台警告,提示等等
// 因为咱们后期会引入一些第三方的库,这时候这些库大部分使用的是旧的react语法,导致警告提示一堆
root.render(
  // <React.StrictMode>
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>
  // </React.StrictMode>
);


