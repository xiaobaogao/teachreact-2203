// 组件声明方式两种
// 一般为了区分组件和js 文件,所以在react 中组件的后缀一般使用.jsx ,当然使用.js  也可以

// 创建一个函数组件
// 特点1: 函数组件无状态 无生命周期
// 无状态 表示 无data数据
// 无生命周期 函数组件没有生命周期函数
// 特点2: 有hooks 函数 
// hooks 表示钩子函数,可以帮助函数组件实现有状态, 有生命周期等等功能,属于函数组件的辅助函数
// 特点3: 函数组件中没有this,this为undefined


//01 第一步: 需要引入 react.js
// 注意: 在react版本17.0.2 后,创建组件的时候就不需要再引入 React,引入也无罪
import React from 'react'

//02: 第二步: 创建函数组件
// 1. 组件名首字母一般大写
// 2. 函数必须有返回值,返回值为页面的模板结构
// 3. 组件返回的页面模板必须有一个父元素
// 4. 在react中可以使用一个包裹的元素代替这个jsx中的父节点,起到一个包裹作用 
// <></> 空标签  和 <React.Fragment></React.Fragment> 类似于vue中的<template></template/> 和<block></block>


// function Fun() {
//     return <React.Fragment>
//         <p>我是一个函数组件</p>
//     </React.Fragment>
// }


// 函数组件实现父子组件传参
// 步骤:
// 第一步: 在父组件中给子组件设置自定义属性,属性值就是要传递的参数
// 第二步: 在子组件中,函数的形参props 就是父组件传递过来的参数对象. 访问该参数 props.属性名



// 引入子组件 FunChild
import Funchild from './FunChild'
const Fun = () => {
    const msg = '李响'
    const nameArr = ['吉永成', '李旺源', '杜文静'];
    return <React.Fragment>
        <p>我是一个函数组件</p>
        {/* 使用函数子组件 */}
        <Funchild tit={msg} id='100' nameArr={nameArr}></Funchild>
    </React.Fragment>
}

//03: 导出该组件
export default Fun
