import React, { Component } from 'react';

// 引入高阶函数
import HOC from '../../config/hoc'
class Child extends Component {
    render() {
        return (
            <div>
                <p>child组件 --{this.props.username}</p>
            </div>
        );
    }
}

export default HOC(Child);
