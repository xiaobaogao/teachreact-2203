import React, { Component } from 'react';

import Child from './Child'
class Father extends Component {
    state = {
        username: '文永江'
    }
    render() {
        return (
            <div>
                <Child username={this.state.username}></Child>
            </div>
        );
    }
}

export default Father;
