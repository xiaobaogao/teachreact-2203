//  类组件
// 使用es6 中的class 来创建组件
// 特点1: 有状态 有生命周期
// 特点2: 无hooks 钩子函数
// 特点3: 类组件中this 为当前组件的实例对象

import React, { Component } from 'react'
// 类名也就是组件名大写 
// 必须继承 React.Component 父组件,这样就可以使用父组件中属性和方法了
// 使用类组件中的render 函数 导出页面模板,jsx 模板中必须有父元素


// 引入类子组件
// 类组件实现父传子 步骤:
// 步骤1 : 在父组件中给子组件设置自定义属性,属性值就是要传递的数据
// 步骤2: 在子组件中通过this.props 可以获取到传递的属性对象,访问方式 this.props.属性名

import Clschild from './ClsChild'
class Cls extends Component {
    state = {
        username: '翠花'
    }
    render() {
        return <>
            <p>我是类组件</p>
            {/* 使用类子组件 */}
            <Clschild username={this.state.username}></Clschild>
        </>
    }
}

// 导出类组件
export default Cls
