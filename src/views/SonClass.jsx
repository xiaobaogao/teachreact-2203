// 子类组件

import React, { Component } from 'react';

class Sonclass extends Component {
    render() {
        return (
            <div>
                <p>子类组件--{this.props.msg}</p>
            </div>
        );
    }
}

export default Sonclass;
