// 函数组件 -- 父组件
// 注意: 函数组件和类组件是可以互相嵌套使用的,当涉及到父传子的时候,
// 接收参数的形式决定于子组件,子组件是函数组件的话,就通过参数props接收,
// 如果子组件是类组件,就通过 this.propsj接收


import React from 'react';
// 引入子类组件
import Sonclass from './SonClass';
const Fatherfun = () => {
    return (
        <div>
            <p>我是父函数组件</p>
            {/* 在函数父组件中使用子类组件 */}
            <Sonclass msg='程文喧'></Sonclass>
        </div>
    );
}

export default Fatherfun;

