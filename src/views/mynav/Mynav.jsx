import React, { Component } from 'react';
import {withRouter} from 'react-router-dom'
// 自定义的导航组件 ,用来替换 Link组件的
class Mynav extends Component {
    render() {
        // console.log('Mynav',this);
        const text = this.props.children
        const url = this.props.to
        const Tag = this.props.tag?this.props.tag:'a'
        return (
            <>
              <Tag onClick={this.go.bind(this,url)}>{text}</Tag>
            </>
        );
    };
    go(url){
        // console.log(this);
       this.props.history.push(url)    
    }
}

export default withRouter(Mynav);
