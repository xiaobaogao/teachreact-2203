// redux 实现的步骤":
// 01: import {createStore} from 'redux'
// 02: 定义初始的state数据
// const defaultState = {count:100,num:0}
// 03: 定义reducer函数 用来操作store中的state中的数据的
// const reducer = (state=defaultState,actions){根据接收actions任务来修改state数据,主要要保留原数据} 
// 04:  创建store实例对象  
// const store = createStore(reducer[,一行配置可以在浏览器的redux插件中调试redux])
// 05: 导出 store 
// 06: 使用 react-redux 辅助插件 来操作redux 
// import {connect,Provider} from 'react-redux'
// <Provider store ={store} ><App/></Provider>


//07:  在页面中使用redux数据并操作
// 01: 在类组件页面操作
// 使用辅助函数 
// function  mapStateToProps(state){
//    return state
// }
// function  mapDispatchToProps(dispatch) {
//     return {addCount(){dispatch(actions)}}
// }
// connect(mapStateToProps,mapDispatchToProps)(Home页面组件)

// 02: 在函数组件页面操作(新内容马上讲的)
// 导入操作redux 的hook函数
// import {useSelector,useDispatch} from 'react-redux';

// 此时变量 state 就是从store中获取的state 数据
// const state = useSelector((state)=>{return state})

// 使用 useDispatch 来获取dispatch方法来用它排除修改store数据的任务
// const dispatch = useDispatch()
// 派发修改任务
// dispatch({type:'jia',payload:100})

import React from 'react';
// 导入操作redux 的hook函数
import {useSelector,useDispatch} from 'react-redux';
const Home = () => {
    // useSelector中的参数函数中的形参 state就是store中的state 数据
    // 此时变量 state 就是从store中获取的state 数据
    const state = useSelector((state)=>{return state})
    console.log(state);

    // 使用 useDispatch 来获取dispatch方法来用它排除修改store数据的任务
    const dispatch = useDispatch()

    const addNum = ()=>{
       // 修改store中的shop中的num数据,唯一的途径通过dispatch 派发一个修改任务,reducer接收修改,修改state
        dispatch({type:'jia',payload:100})
    }
    return (
        <div>
            <p>store中的shop模块中的num:{state.shop.num}</p> 
            <p> <button onClick={addNum}>修改store中的shop中的num</button></p>
        </div>
    );
}

export default Home;
