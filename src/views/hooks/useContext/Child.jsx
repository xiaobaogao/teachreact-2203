import React,{useContext} from 'react';

// useContext 
// 作用: 实现跨级组件数据的共享

// 引入 context 对象
import contextObj from '../../../config/context';

const Child = () => {
   // 使用useContext 解构出对应的数据,data 变量就是解构出来的数据.
   // 注意: 函数组件中消费祖先组件共享的数据方式是使用useContext,和类组件不同, 类组件有2种方式

   const data = useContext(contextObj)
  console.log(data);
    return (
        <div>
            <p>子组件--{data}</p>
        </div>
    );
}

export default Child;
