import React,{useState} from 'react';

import Child from './Child'

// 引入 context 对象
import contextObj from '../../../config/context';
// 解构出 <provider> 组件
const  {Provider} =  contextObj

const Father = () => {
    const [count,setCount] = useState(66)
    return (
        <div>
            <p>父组件</p>
            {/* 使用子组件 */}
            <Provider value ={count}>
                <Child></Child>
            </Provider>
        </div>
    );
}

export default Father;
