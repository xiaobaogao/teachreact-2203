

import React,{useEffect,useState} from 'react';
// useEffect 
// 作用: 用来模拟类组件的生命周期的
const Father = () => {
    let [user,setUser] = useState({name:"阴万成",age:20})
    let [nameArr,setNameArr] = useState(['吴泽华','张杰'])
    // 情形1:
    // 执行世间: 初始化执行. 数据更新 也执行
    // 对标 render 生命周期
    // useEffect(() => {
    //    console.log(100);
    // });

    // 情形2:
    // 执行时间: 初始化执行,数据更新 先执行箭头函数,在初始化执行
    // useEffect(() => {
    //     console.log(100);
    //     const timer = setInterval(() => {
    //         console.log('定时器');
    //     }, 1000);
    //     return () => {
    //        console.log(200);
    //        clearInterval(timer)
    //     };
    // });

    // 情形3:
    // 参数1: 为函数
    // 参数2: 数组,用来存放当前组件的数据变量,来监听该组件的数据变量,当发生变化时,触发我的生命周期函数
    // 特点: 只有初始化得时候执行 ,return 可省略
    // 对标 constructor 生命周期
    // useEffect(() => {
    //    console.log(100);
    //     // return () => {
    //     //  console.log(200);
    //     // };
    // }, []);

    //情形4:
    //参数2 有值
    //当参数2中变量发生变化, 就是执行retuen 函数,然后再执行初始化操作
    useEffect(() => {
        console.log(100);
        return () => {
            console.log(200);
        };
    }, [user]);
    
    const editUser = ()=>{
        setUser({
            ...user,
            age:user.age+1
        })
    }

   const addUser = ()=>{
       setNameArr(()=>{
           return [...nameArr,'赵宁宁']
       })
   }
    return (
        <div>
            <p>{user.name} --{user.age}</p>
            <p><button onClick={editUser}>age++</button></p>
            <ul>
                {nameArr.map((item,index)=><li key={index}>{item}</li>)}
            </ul>
            <p><button onClick={addUser}>添加成员</button></p>
        </div>
    );
}

export default Father;
