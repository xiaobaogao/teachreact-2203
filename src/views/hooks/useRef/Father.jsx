import React,{useRef,useState,createRef} from 'react';

//  useRef
// 作用: 用来给标签或组件绑定ref 引用, 这样就可以获取标签和组件实例了
// 对比类组件中的createRef比函数组件的useRef 好用,


// useRef 和createRef 区别
// 01: 相同点: 都是用来创建ref 引用对象的,然后将该引用对象给普通标签和组件设置ref 属性的
//02: 不同点: useRef() 在初始化执行,数据更新时,不会执行  createRef() 方法当当前组件的数据更新后,他会重复执行

import Child from './Child'
const Father = () => {
     let [num1,setNum1] = useState(0)
    //  创建ref 引用对象
    const ref1 = useRef()
    const ref2 = useRef()

    const getRef = ()=>{
        console.log(ref1.current.innerHTML);
        console.log(ref2);
        // 将子组件child中的num 变量赋值给当前父组件中的num1变量
        setNum1(ref2.current.num)
    }
    return (
        <div>
            <p ref={ref1}>我是父组件 --{num1}</p>
            <p><button onClick={()=>{ref2.current.addNum()}}>在父组件中调用子组件的addNum</button></p>
            {/* 使用子组件 */}
            <Child ref={ref2}></Child>
            <p><button onClick={getRef}>获取ref</button></p>
        </div>
    );

}

export default Father;
