import React,{forwardRef,useState,useImperativeHandle} from 'react';
// forwardRef 该方法是一个高阶组件强化函数hoc 
// useImperativeHandle 该hooks 函数 可以将当前函数组件中的数据和对应的数据操作方法抛出给父组件
// useImperativeHandle(()=>{return 抛出的数据和方法})
const Child = (props,ref) => {
    let [num,setNum] = useState(50);
    const addNum = ()=>{
        setNum(num+1)
    }

    // 抛出去了
    useImperativeHandle(ref,()=>{return {
        num,addNum
    }})
    return (
        <div>
            <p>我是子组件 --{num}</p>
            <p><button onClick={addNum}>num++</button></p>
        </div>
    );
}

export default forwardRef(Child) ;
