
// hooks 的使用限制
//01: hooks 函数 作为辅助函数 只能在函数组件中使用,不能再函数组件外使用,也不能在类组件中使用,可以在其他的hook函数

//02: hook 不能被有条件的使用,就是不能在条件语句中使用hooks
import React,{useState} from 'react';

const Father = () => {

    // 01: 基础数操作
    let [count, setCount] = useState(0);
    // count 表示变量名 值为 0 
    // setCount 为修改count变量的方法函数,并且只能通过该方法修改count
    // useState(0); 该方法的参数0为初始变量

    const addCount = ()=>{
        // 修改count
        // setCount(100)
        // ++
        // 第一种方式
        // setCount(count++)
        // 第二种方式 
        setCount(()=>{
            count++
            return count
        })
    }

    //02: 对象形式数据
    let [user, setUser] = useState({
        username:"平凡琪",
        age:18
    });

    const addAge = ()=>{
        // 方式不可以
        // user.age++
        // console.log(user);
        // setUser(user)  因为 参数user 和变量名一致,所以 usestate 认为该数据没有被修改,所以不会重新渲染
        
        //方式1 可以
        // let newObj = {
        //     username:user.username ,
        //     age:user.age+1
        // }
        // setUser(newObj) 

        //方式2 可以
        // setUser(()=>{
        //     return {
        //         ...user,
        //         age: user.age+1
        //     }
        // })
    }

    //03: 操作数组数据
    let [nameArr, setNameArr] = useState(['程文喧','牛衍娇']); 
    const addUser = ()=>{

        //该方法不可行
        // nameArr.push('刘建芳')
        // console.log(nameArr);
        // setNameArr(nameArr)

        // 使用函数方式
        setNameArr(()=>{
            return [...nameArr,'刘建芳']
        })
    }

    return (
        <div>
            <p>函数组件</p>
            <p>count:{count}</p>
            <p><button onClick={addCount}>修改count</button></p>
            <hr/>
            <p>{user.username}--{user.age}</p>
            <p><button onClick={addAge}>修改age</button></p>
             <hr/>
            <ul>
                {
                  nameArr.map((item,index)=>{
                      return <li key={index}>{item}</li>
                  })  
                }
            </ul>
            <p><button onClick={addUser}>添加人员</button></p>
        </div>
    );
}

export default Father;
