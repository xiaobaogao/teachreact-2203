import React from 'react';
import {useLocation,useParams} from 'react-router-dom'
const News = () => {
     
     const loc = useLocation() // 等同于 类组件中的this.props.location
     // 获取参数  查询字符串方式的参数和隐式传参的方式
     console.log(loc);
    return (
        <div>
            <p>新闻页面</p>
        </div>
    );
}

export default News;
