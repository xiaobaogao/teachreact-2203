import React from 'react';
import {Link,Route,useHistory,useLocation,useParams} from 'react-router-dom'
// 引入对应的页面组件
import News from './News'
import About from './About'



// 定义的根组件
const Index = () => {
    const his = useHistory() // 等同于类组件中的this.props.history
    const loc = useLocation() // 等同于 类组件中的this.props.location
    const par = useParams()  // 等同于类组件中的this.props.match.params

    // 定义跳转页面事件
    const go = (url)=>{
        // console.log('go');
        // 以前在类组件怎么实现编程式路由跳转 this.props.history.push(url)  this.props.history.go(-1)
        his.push(url)
    }


    return (
        <div>
            {/* 导航标签 */}
            {/* <p>
                <Link to='/news'>新闻</Link>
                <Link to='/about'>关于</Link>
            </p> */}
            {/* 编程式导航 */}
            <p>
                {/* <span onClick={()=>go('/news?id=99')}>新闻</span> */}
                 <span onClick={()=>go({pathname:'/news',state:{a:1,b:2}})}>新闻</span>
                <span onClick={()=>go('/about/'+9999)}>关于</span>
            </p>

            {/* 路由组件的坑 */}
            <Route path='/news' component={News}></Route>
            <Route path='/about/:id?' component={About}></Route>
        </div>
    );
}

export default Index;
