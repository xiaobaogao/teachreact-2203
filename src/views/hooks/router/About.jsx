import React from 'react';
import {useParams} from 'react-router-dom'
const About = () => {
    const par = useParams()  // 等同于类组件中的this.props.match.params
    // 获取restful风格的参数的
    console.log(par);
    return (
        <div>
            <p>关于页面</p>
        </div>
    );
}

export default About;
