import React, { useState, useEffect } from 'react';
//  目的:要实现获取当前浏览器的网络状态
// BOM  操作浏览器的 5个
// window screen  location history navigator
// navigator.onLine 该属性的值 为ture 或false  true 表示在线  false 表示离线
// DOM  操作DOM 节点的

// 实时监听当前浏览器的网络状态 
// 使用window绑定一个online 事件 ,当在线的时候 触发该事件
// 使用window绑定一个offline 事件 ,当离线的时候 触发该事件

// 导入网络状态的hook函数
import useLine from './useLine';
const Myhook = () => {
    return (
        <div>
            <p>当前的网络状态为: {useLine() ? '在线' : '离线'}</p>
        </div>
    );
}

export default Myhook;
