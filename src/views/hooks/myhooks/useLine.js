// 自定义当前的hook

//01: 本质上是一个函数
//02: 所有的hooks 函数都是以use开头
//03:功能 实现可以获取当前浏览器的网络状态的hook函数

import { useState, useEffect } from 'react'
const useLine = () => {
    let [line, setLine] = useState(navigator.onLine)
    //console.log(navigator.onLine)

    // 定义事件处理函数
    const onlineFn = () => {
        console.log('在线')
        setLine(true)
    }
    const offlineFn = () => {
        console.log('离线')
        setLine(false)
    }
    useEffect(() => {
        window.addEventListener('online', onlineFn)
        window.addEventListener('offline', offlineFn)

        return () => {
            window.removeEventListener('online', onlineFn)
            window.removeEventListener('offline', offlineFn)

        }
    })




    // 返回一个当前的浏览器的网络状态
    return line
}

export default useLine
