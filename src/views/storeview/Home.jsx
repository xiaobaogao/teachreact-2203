import React, { Component } from 'react';

// 将store中的数据展示在当前页面中
// 01:先引入store 对象
// 02: 将store中的数据给当前组件的state,这样就可以操作使用state数据了
import store from '../../store'
console.log(store);
// console.log(store.getState());
class Home extends Component {
    constructor() {
        super()
        this.state = store.getState();

        // 重新订阅store中的数据,只要store中的数据发生变化,就触发subscribe 方法.然后重新
        // 给当前组件的state 赋值
        // store.subscribe(()=>{
        //     console.log('subscribe');
        //    this.setState(()=>{
        //        return store.getState();
        //    })
        // })

        // 如上的重新订阅方法还可以简写
        store.subscribe(()=> this.setState(()=> store.getState()) )  
              
       
    }
    // state = store.getState()
    render() {
        return (
            <div>
                <p>我是首页</p>
                <p>store中的count: {this.state.count}</p>
                <p><button onClick={this.addCount}>count++</button> </p>
                 <p><button onClick={this.jianCount}>count--</button> </p>
            </div>
        );
    };
    addCount = () => {
        // 修改store中的数据, 使用store中dispatch方法派发一个action任务 
        // 语法: store.dispatch(action)
        store.dispatch({
            type: '+',
            payload: 1
        })
    };
    jianCount=()=>{
        store.dispatch({
           type:'-',
           payload:1 
        })
    }
}

export default Home;
