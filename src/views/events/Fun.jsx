
// 函数组件
import React from 'react';
// 01:
// 函数组件中绑定的事件执行函数,既可以写在函数组件内,也可以写在函数组件外,
// 注意: 一般来说 事件执行函数一般都写在函数组件内,因为 函数组件如果使用hooks辅助函数的话,只能在函数组件
// 内使用,如果事件执行函数中使用了hooks 钩子函数,那么只能写在函数组件内

// 02: 事件对象
// 注意1: react中的事件对象为合成事件对象,因为该事件对象是react对浏览器原生事件对象的一个封装,
// 合成事件比原生事件对象有更好的浏览器兼容性(不需要考虑兼容性处理)
// 注意2: 合成事件对象课原生事件对象用法一直 也有组件冒泡 默认行为等等用法 
// 获取原生事件对象:e.nativeEvent

// 03: 事件传参
// 可以将执行函数在行内写成箭头函数的函数的形式
// <p onClick={(e) => handleClick(100, e)}>我是函数组件</p>

//  在函数组件外使用执行函数
// const handleClick = () => {
//     console.log('点击事件');
// }

const Fun = () => {
    // 在函数组件内写执行函数
    const handleClick = (num, e) => {
        console.log('点击事件');
        //参数
        console.log(num);
        // 事件对象
        console.log(e); //undefined
    }
    return (
        <div>
            <p onClick={(e) => handleClick(100, e)}>我是函数组件</p>
        </div>
    );
}

export default Fun;
