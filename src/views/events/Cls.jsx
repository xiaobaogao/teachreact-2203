
//  类组件
import React, { Component } from 'react';

// 注意: 类组件如果constructor 则必须写super() 否则报错
// 类组件绑定事件的方式一共有3种 ,注意最常用的是类成员属性的方式绑定事件

// 类组件中的this 指向问题
// 01: 第一种方式: 
// 在行内属性上使用箭头函数的形式绑定的事件 (this指向组件实例对象,且可以传参)
// 02: 第二种方式:
// 在行内属性上直接使用this.执行函数,执行函数表达式为普通函数  (this为undefined,不能传参)
// 03: 第三种方式:
// 在行内属性上直接使用 this.执行函数,执行函数的表达式为箭头函数(this为组件实例对象,不能传参)
// 04 : 第四种方式:
// 在构造函数中给事件执行函数绑定this实例 (this为组件实例,可以传参)
// 05: 第五种方式
// 在行内属性上直接绑定this (this为组件实例,可以传参)

// 事件执行函数写在组件外
// const handleClick = (num, e) => {
//     console.log('点击事件');
//     console.log(num);
//     console.log(e);
//     console.log(this); // undefined  不能访问props 属性 和state 数据
// }

class Cls extends Component {
    constructor() {
        super()
        // 在初始化constructor中设置赋值事件执行函数
        // this.handleClick = (num, e) => {
        //     console.log('点击事件');
        //     console.log(num);
        //     console.log(e);
        //     console.log(this); // 组件实例对象 就可以访问该实例中的props 属性 和state 数据
        // }
        // console.log(this); // 组件实例对象
        // this.handleClick = this.handleClick.bind(this, 100)
    }
    //  以类成员属性的方式添加事件执行函数
    // handleClick = (num, e) => {
    //     console.log('点击事件');
    //     console.log(num);
    //     console.log(e);
    //     console.log(this); // 组件实例对象 就可以访问该实例中的props 属性 和state 数据
    // }

    handleClick(num, e) {
        console.log('点击事件');
        console.log(e);
        console.log(num);
        console.log(this); // 组件实例对象 就可以访问该实例中的props 属性 和state 数据
    }

    render() {
        return (
            <div>
                {/* 第一种写法:行内使用箭头函数: 比较常用 (有this和参数)*/}
                <p onClick={(e) => this.handleClick(100, e)}>我是类组件1</p>
                {/* 第二种写法:函数为普通函数写法 (没有this,没有参数)*/}
                <p onClick={this.handleClick}>我是类组件2</p>
                {/* 第三种写法:比较常用 函数为箭头函数写法 (有this,没有参数)*/}
                <p onClick={this.handleClick}>我是类组件3</p>
                {/* 第四种写法:在构造函数中绑定 this (有this,有参数)*/}
                <p onClick={this.handleClick}>我是类组件4</p>
                {/* 第五种写法: 比较常用 直接行内绑定this (有this,有参数)*/}
                <p onClick={this.handleClick.bind(this, 99)}>我是类组件5</p>
            </div>
        );
    }
}

export default Cls;
