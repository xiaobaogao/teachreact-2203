import React, { Component } from 'react';

class Category extends Component {
    constructor(props) {
        // 该props 参数表示该组件的props 属性对象,可以 存放父传子的参数,还可以显示路由参数
        super()
        console.log(props);
    }
    render() {
        return (
            <div>
                <p>分类页</p>
            </div>
        );
    }
}

export default Category;
