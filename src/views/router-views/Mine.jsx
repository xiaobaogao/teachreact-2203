import React, { Component } from 'react';
import { Route, Redirect, Switch } from 'react-router-dom'
import Mine2 from './Mine2';
import Mine1 from './Mine1';
import Notfind from './Notfind';
class Mine extends Component {
    render() {
        return (
            <div>
                <p>我的页</p>
                {/* 定义二级路由对应的坑,react中的二级路由对应的path 必须加上一级路由的path  */}
                <Switch>
                    <Route path='/mine/mine1' component={Mine1}></Route>
                    <Route path='/mine/mine2' component={Mine2}></Route>
                    <Redirect from='/mine' to='/mine/mine1' exact></Redirect>
                    {/* 404路由 */}
                    <Route component={Notfind}></Route>
                </Switch>
            </div>
        );
    }
}

export default Mine;
