import React, { Component } from 'react';

// 通过模块化的方式导入图片
import NotImg from '../../assets/img/404.jpeg';

class Notfind extends Component {
    render() {
        return (
            <div>
                <img src={NotImg} />
            </div>
        );
    }
}

export default Notfind;
