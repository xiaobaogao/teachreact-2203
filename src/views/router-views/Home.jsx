import React, { Component } from 'react';
// 引入home组件的样式 这种方式是最常用的css 方式
// import '../../assets/css/home.css'

// 按需导入对应的css 样式
import {Title,Title2,Title3} from '../../assets/style/home';
import axios from 'axios'
class Home extends Component {
    constructor() {
        super()
        console.log('home',this);
    }
    render() {
        return (
                <div className='home' style={{fontSize:'50px'}}>
                    <Title>
                     <p>Home页</p>
                   </Title>
                   <Title2>
                     <p>样式的继承</p>
                    </Title2>
                    <Title3 color='yellow'>
                      <p>属性的传参</p>
                    </Title3>
                </div>      
        );
    };
    componentDidMount(){
        // axios.get('https://api.i-lynn.cn/college').then(res=>{
        //     console.log(res);
        // })  
          axios.get('/api').then(res=>{
            console.log(res);
        })  
    }
}


export default Home;
