import React, { Component } from 'react';

// this.setState() 两种方式的区别
// 第一种对象方式
// 如果修改的属性数据一致的话, 那么独享写法会将多次修改合并,最后一次的修改覆盖前面的修改操作

// 第二种函数方式
// 如果修改的属性数据一致的话, 函数不会合并,将这几个操作放在一个数组中,三个箭头函数会循环依次执行,所以会依次执行

class Cls extends Component {
    state = {
        username: "张永超",
        age: 18
    }
    render() {
        const { username, age } = this.state
        return (
            <div>
                <p>{username}--{age}</p>
                <p onClick={this.handle1}>对象形式++</p>
                <p onClick={this.handle2}>函数形式++</p>
            </div>
        );
    };
    handle1 = () => {
        this.setState({
            age: this.state.age + 1
        })
        this.setState({
            age: this.state.age + 1
        })
        this.setState({
            age: this.state.age + 1
        })
    };
    handle2 = () => {
        this.setState((state) => {
            return {
                age: state.age + 1
            }
        })
        this.setState((state) => {
            return {
                age: state.age + 1
            }
        })
        this.setState((state) => {
            return {
                age: state.age + 1
            }
        })
    }
}

export default Cls;
