import React, { Component } from 'react';

// 总结: redux 基本操作中和vuex相比一些弊端
// 01: redux 中的store 数据发生变化,页面没有实时响应,换需要页面重新通过 subscribe() 重新订阅store中的数据
// 02:  页面中需要使用store中的数据,那么还需要引入store,这样又增加额外的操作,vuex 直接将store 挂载到根实例
// 这样每个页面的子组件都能通过原型链的方式访问到根实例,从而获取store  (this.$store.state.数据)

// 鉴于以上两点较vuex 使用麻烦,react又有了第三个插件 react-redux
// 该插件主要是用来辅助操作redux的
// 使用时需要下载安装 npm i react-redux


// 在页面中获取store中的数据
//在当前页面组件中引入react-redux 中提供的connect方法,该方法是一个高阶组件函数
import { connect } from 'react-redux'
class Home extends Component {
    render() {
        return (
            <div>
                <p>Home页</p>
                {/* 获取store中的state数据 */}
                <p>store中的num: {this.props.shop.num}</p>
                <p>
                    <button onClick={this.props.addNum}>num++</button>
                </p>
            </div>
        );
    }
}

// 创建一个mapStateToProps方法 ,该方法可以将store中的数据映射到当前组件的props属性上
// 必须有返回值,返回值值为store中的state数据
// 类似于veux中的mapState 将store中的数据映射到当前组件上
function mapStateToProps(state) {
    // console.log(state);
    return {
        shop: state.shop
    }
}

// 创建一个mapDispatchToProps方法,该方法将所有的dispatch 方映射到当前组件的props属性上
// 必须有返回值,返回值为操作store中state的方法
// 类似于vuex 中的mapMutations 或者mapActions 将store中方法映射到当前组件的method对象上
function mapDispatchToProps(dispatch) {
    return {
        addNum() {
            dispatch({ type: 'jia', payload: 10 })
        }
    }
}


// 使用高阶组件中的高阶函数处理
export default connect(mapStateToProps, mapDispatchToProps)(Home);
