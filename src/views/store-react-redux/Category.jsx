import React, { Component } from 'react';

import { connect } from 'react-redux'
class Category extends Component {
    render() {
        return (
            <div>
                <p>分类页</p>
                {/* 使用user模块中的数据 */}
                <p>store中的count: {this.props.user.count}</p>
                <p onClick={this.props.addCount}><button>count++</button></p>
            </div>
        );
    }
}

// 将store中的user模块中的数据映射到当前组件自身的props属性上
function mapStateToProps(state) {
    return {
        user: state.user
    }
}

// 将操作user模块中国的方法映射到当前组件自身的props 属性上

function mapDispatchToProps(dispatch) {
    return {
        addCount() {
            dispatch({ type: '+', payload: 1 })
        },
        jianCount() {
            dispatch({ type: '-', payload: 1 })
        }
    }

}


// connect(mapStateToProps,mapDispatchToProps) 整体的函数返回值的结果为一个HOC 高阶函数
// 该高阶强化函数 将 store中的数据和操作store数据的方法映射到组件自身的props 属性上
export default connect(mapStateToProps, mapDispatchToProps)(Category);
