//  类组件
// 因为 函数组件是无状态无生命周期的,所以 咱们以类组件为例讲解 state
// 01: 类组件的state 
// state 就是类比于vue 组件中的data ,也就是说 state为当前组件的私有数据
// state 初始化声明2种写法:
// 第一种: 在构造函数中声明state
// 第二种: 在组件的属性上直接设置state (最常用)

// 02: 修改state
//  vue 是通过循环给data对象中的所有数据设置 Object.defineProperty()方法设置get 和set方法,然后当数据被修改时,触发set方法
//  这样就实现了数据的监听,然后在操作dom 重新渲染页面
// react 如果直接使用vue 语法直接赋值,这样数据没有被react监听到,无法监听所有无法触发修改,react中有自己一套东西,通过
// this.setState({数据:值}) 修改,这样就可以被监听到了
// 注意1: setState采用的的对象合并的语法,不会将state中的其他数据覆盖,后面的redux他是覆盖
// 注意2: setState方法属于异步操作, 要想实时获取数据,那么需要他的第二个参数 回调函数
// this.setState(update,callback)
// 注意3: 在react18之前 setState({}) 可以实现同步方式,将setState() 写在定时器或原生事件中,但是在react18 以后,使用定时器和原生事件中不能再实现同步方式

// this.setState() 两种写法
// 第一种:对象是的写法 
// this.setState({key:value},callback) 

// 第二种:函数式的写法
// this.setState((state,props)=>{return{key:value}},callback)
// 参数1: state 就是当前组件的数据state 对象
// 参数2: props 就是当前组件实例接收的父组件传递的参数


import React, { Component } from 'react';
class Cls extends Component {
    constructor() {
        super()
        // this.state = {
        //     username: "张峻理",
        //     age: 18,
        //     nameArr: [
        //         {
        //             id: 1,
        //             name: "杨佳菲"
        //         },
        //         {
        //             id: 2,
        //             name: "姜海林"
        //         }
        //     ]
        // }
    }
    state = {
        username: "张峻理",
        age: 18,
        nameArr: [
            {
                id: 1,
                name: "杨佳菲"
            },
            {
                id: 2,
                name: "姜海林"
            }
        ]
    }
    render() {
        return (
            <div>
                <p>我是类组件</p>
                {/* 使用username */}
                <p>{this.state.username}--{this.state.age}</p>
                {/* 遍历数组 */}
                <ul>
                    {this.state.nameArr.map(item => <li key={item.id}>{item.name}</li>
                    )}
                </ul>
                <p><button onClick={this.editusername}>修改state数据--username</button></p>
                {/* 使用原生js事件 */}
                <p className='p1'>原生点击事件</p>
            </div>
        );
    };
    componentDidMount() {
        // console.log(this);
        // 组件的生命周期函数 类似于vue中的mounted
        // console.log(document.querySelector('.p1'));
        // document.querySelector('.p1').onclick = () => {
        //     console.log('原生事件');
        //     this.setState({
        //         username: '张铭娜'
        //     }, () => {
        //         console.log(1, this.state.username); // 张铭娜
        //     })

        //     console.log(2, this.state.username); // 张峻理

        // }

    }
    // 修改username事件
    editusername = () => {
        console.log(this);
        // this.state.username = '张铭娜'

        // 第一种方式修改
        // this.setState({
        //     username: '张铭娜'
        // }, () => {
        //     console.log(1, this.state.username);
        // })
        // console.log(2, this.state.username); 

        // 第二种方式:
        this.setState((state, props) => {
            // console.log(state);
            return {
                username: '张铭娜',
                age: state.age + props.count
            }
        }, () => {
            console.log(this.state);
        })
    }

}

export default Cls;
