import React, { Component } from 'react';

// 引入子组件Cls
import cls from './cls'
class Father extends Component {
    state = {
        count: 10
    }
    render() {
        return (
            <div>
                <p>我是父组件</p>
                <cls count={this.state.count}></cls>
            </div>
        );
    }
}

export default Father;
