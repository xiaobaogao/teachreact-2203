//类子组件
// 快捷方式创建类组件 
// rcc 方式创建类组件

// 
import React, { Component } from 'react';
class Clschild extends Component {
    render() {
        console.log('Clschild', this);
        return (
            <div>
                <p>我是类子组件</p>
            </div>
        );
    }
}

export default Clschild;

