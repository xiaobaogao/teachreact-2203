import React, { Component } from 'react';

import eventBus from '../../config/events'
class Child2 extends Component {
    state = {
        username: '熊二',
        str: ''
    }
    render() {
        return (
            <div>
                <p>child2--{this.state.username}</p>
                {this.state.str ? <p>熊大对熊二说--{this.state.str}</p> : ''}
            </div>
        );
    };
    componentDidMount() {
        eventBus.on('tranfser', (data) => {
            console.log(data);
            this.setState({
                str: data
            })
        })
    }

}

export default Child2;
