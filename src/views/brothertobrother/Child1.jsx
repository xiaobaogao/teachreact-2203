import React, { Component } from 'react';

// 使用兄弟组件传参,需要使用到一个第三方的模块或者插件 events
// 引入 event.js
// 
import eventBus from '../../config/events'
console.log(eventBus);
class Child1 extends Component {
    state = {
        username: '熊大',
        say: '光头强又来砍树啦'
    }
    render() {
        return (
            <div>
                <p>child1--{this.state.username}</p>
                <p onClick={this.emitFn}>点击事件</p>
            </div>
        );
    };
    emitFn = () => {
        // 将child1中的数据传递给child2组件
        eventBus.emit('tranfser', this.state.say)
    }
}

export default Child1;
