import React, { Component } from 'react';

import Child1 from './Child1';
import Child2 from './Child2';
class Father extends Component {
    render() {
        return (
            <div>
                {/* 兄弟组件1 */}
                <Child1></Child1>
                {/* 兄弟组件2 */}
                <Child2></Child2>
            </div>
        );
    }
}

export default Father;
