import React, { Component } from 'react';

// 子传父
// 01: 在父组件中定义一个方法,该方法接收一个形参,然后将该方法传递给子组件,子组件接收到该方法后,调用,
// 将子组件的数据作为实参传递给该方法,这样,父组件中的形参就是子组件要传递的数据


// 02: 在子组件中定义一个方法,该方法的返回值为子组件中的数据,然后在父组件中通过ref属性调用子组件上的这个方法,
// 那么返回值就是子组件上的数据

// 03: 直接通过ref 获取到子组件上的state 数据

class Child extends Component {
    state = {
        userinfo: {
            name: '林佳',
            age: 20
        }
    }
    render() {
        return (
            <div>
                <p>我是子组件</p>
            </div>
        );
    };
    componentDidMount() {
        this.props.getdatafromchild(this.state.userinfo)
    };
    giveDatatoFather() {
        return this.state
    }
}

export default Child;
