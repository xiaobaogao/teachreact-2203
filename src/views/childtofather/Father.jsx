import React, { Component, createRef } from 'react';

import Child from './Child';
class Father extends Component {
    ref1 = createRef();
    render() {
        return (
            <div>
                <Child getdatafromchild={this.getdatafromchild} ref={this.ref1}></Child>
            </div>
        );
    };
    getdatafromchild(data) {
        console.log(data);
    };
    componentDidMount() {
        console.log(this.ref1.current.giveDatatoFather());
    }
}

export default Father;
