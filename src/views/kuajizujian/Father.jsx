import React, { Component } from 'react';
// 跨级组件传参
// vue 中也有类似的语法:  
// vue 也有一个跨级组件的数据解决方法 provide + inject (基本不用,因为不好用,数据实现响应式比较麻烦)

//react 跨级组件
// 第一步: 使用createContext方法,创建context对象
// 第二步: 导入该context对象
import context from '../../config/context';
import Son from './Son';
const { Provider } = context
class Father extends Component {
    state = {
        msg: '毛泽东'
    }
    render() {
        return (
            <div>
                <Provider value={this.state.msg}>
                    <Son></Son>
                </Provider>
                <p><button onClick={() => { this.setState({ msg: "领袖" }) }}>修改msg</button></p>
            </div>
        );
    }
}

export default Father;
