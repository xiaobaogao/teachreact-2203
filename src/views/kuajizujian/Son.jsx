import React, { Component } from 'react';

import context from '../../config/context';
import Grandson from './Grandson';
const { Consumer } = context;

class Son extends Component {
    render() {
        return (
            <div>
                <p>儿子组件</p>
                <Consumer>
                    {
                        (value) => {
                            {/* console.log(value) */ }
                            return <p>{value}</p>
                        }
                    }
                </Consumer>
                <Grandson></Grandson>
            </div>
        );
    }
}

export default Son;
