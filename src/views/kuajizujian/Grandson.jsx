import React, { Component } from 'react';

import context from '../../config/context';
class Grandson extends Component {
    // 使用静态属性 contextType接收context 对象,内部会将context 包含数共享据映射到当前组件的实例上,会在
    // 组件上添加一个context属性,属性的值就是共享的数据
    static contextType = context;
    render() {
        console.log(this);  // 当前组件的实例对象
        return (
            <div>
                <p>孙子组件</p>
                {/* 使用共享的数据 */}
                <p>{this.context}</p>
            </div>
        );
    };

}

// Grandson.contextType = context

export default Grandson;
