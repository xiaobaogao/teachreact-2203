import React, { Component } from 'react';

// 在子组件中引入 props-type
import pts from 'prop-types'
// 通过设置静态属性类约束传递的props参数
// 问题: 静态属性和实例属性的区别?
// pts的值为 string ,number ,func ,array,object,.....
// isRequired 该属性不能单独使用只能写在类型后
class Son extends Component {
    // 第一种写法: 在组件内使用静态属性
    // static propTypes = {
    //     num: pts.string.isRequired
    //     // ...
    // }

    // 第一种写法: 设置默认值
    static defaultProps = {
        num: 66
    }
    render() {
        return (
            <div>
                <p>我是子组件</p>
                <p>{this.props.num}</p>
            </div>
        );
    }
}

// 第二种写法: 在组件外 添加静态属性
Son.propTypes = {
    num: pts.number.isRequired
    // ...
}

//第二种写法: 设置默认参数值
// Son.defaultProps = {
//     num: 99
// }



export default Son;

