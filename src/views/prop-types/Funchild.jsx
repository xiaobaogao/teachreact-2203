import React from 'react';

import pts from 'prop-types'
//  在函数组件中对props 属性进行约束
//  使用静态属性设置类型

// 设置props的默认值
// 在组件中通过静态属性设置props默认值

const Funchild = (props) => {
    return (
        <div>
            <p>我是函数子组件 --{props.username}</p>
        </div>
    );
}

// 使用静态属性设置类型
Funchild.propTypes = {
    username: pts.string.isRequired
}
// 设置默认值
Funchild.defaultProps = {
    username: '王强' // 设置的参数username
    // .... 
}

export default Funchild;
