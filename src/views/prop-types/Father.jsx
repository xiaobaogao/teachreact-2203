import React, { Component } from 'react';
// prop-types
// 该插件模块用来对父传子的props属性做限制的, 可以规定该属性的类型,以及是否必须传的参数
// 概述: 该 prop-types在 17版本之前是需要安装下载的, 17之后就不需要下载. 脚手架自带了

// 引入自组件
import Son from './Son';
class Father extends Component {
    state = {
        num: 100
    }
    render() {
        return (
            <div>
                <p>我是父组件</p>
                {/* 使用子组件 */}
                {/* <Son num={this.state.num}></Son> */}
                {/* 没有传参数 */}
                <Son></Son>
            </div>
        );
    }
}

export default Father;
