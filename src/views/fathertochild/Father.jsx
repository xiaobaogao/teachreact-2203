import React, { Component, createRef } from 'react';

// react 中 父传子
// 01: 通过props属性传值,给子组件绑定自定义属性,然后值就是要传递的参数,子组件通过props参数或this.props获取参数

// 02: 在父组件中定义一个函数方法,该方法返回值为当前父组件的state数据,然后将该方法传递给子组件,
// 子组件获取方法后,调用,该调用函数的返回值就是父组件的数据

// 03: 在子组件上定义一个方法,该方法接收一个形参,然后在父组件中,通过给子组件绑定ref属性,这样就可以获取
// 子组件实例对象,进而获取子组件上的数据和方法,调用子组件上绑定的方法,将父组件上的数据作为实参传递,这样子组件上
// 形参就是接收的父组件上的数据

import Child from './Child';
class Father extends Component {
    state = {
        nameArr: ['张宇', '井甲壮']
    }
    ref1 = createRef()
    render() {
        return (
            <div>
                {/* 使用子组件 */}
                <Child giveDataToChild={this.giveDataToChild} ref={this.ref1}></Child>
            </div>
        );
    };
    giveDataToChild = () => {
        return this.state.nameArr
    };

    componentDidMount() {
        this.ref1.current.getDataFromFather(this.state.nameArr)
    }


}

export default Father;
