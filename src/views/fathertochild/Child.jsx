import React, { Component } from 'react';

class Child extends Component {
    state = {
        childdata: []
    }
    render() {
        return (
            <div>
                <p>我是子组件</p>
            </div>
        );
    };
    componentDidMount() {
        // console.log(this.props.giveDataToChild());
        this.setState({
            childdata: this.props.giveDataToChild()
        })
    };
    getDataFromFather(data) {
        console.log(data);
    }
}

export default Child;
