import React, { Component } from 'react';

class Son extends Component {
    render() {
        return (
            <div>
                <p>我是子组件--{this.props.children}</p>
            </div>
        );
    }
}

export default Son;
