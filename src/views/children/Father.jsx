import React, { Component } from 'react';

import Son from './Son'
// children 属性
// 特点:本质上也是一种父传子的方式
// 01:当在父组件中给子组件的标签中间添加内容时,正常是不显示的,这个类似于vue 插槽.
// 在react中 他会在子组件的props属性上加一个children属性,chldren属性的值就是你添加的内容
// 02:注意: 当 子组件标签中间的内容只有一行,那么children 为字符串,当中间内容为多个标签,children 为数组

class Father extends Component {
    state = {
        msg: '我是中间内容',
        nameArr: [
            '王迪',
            '李子杰'
        ]
    }
    render() {
        return (
            <div>
                <p>我是父组件</p>
                {/* <Son>{this.state.msg}</Son> */}
                {/* 中间写多个标签内容 */}
                {/* <Son>
                    <p>2203</p>
                    <p>2204</p>
                </Son> */}
                <Son>{this.state.nameArr}</Son>

            </div>
        );
    }
}

export default Father;
