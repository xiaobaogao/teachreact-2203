import React, { Component, createRef } from 'react';
import Grandson from './Grandson';

// 使用ref
// vue中 ref 属性 绑定在普通标签上<p ref='p1'></p>,通过this.$refs.p1 获取到了普通dom节点
// vue中 ref属性 绑在子组件标签上,可以获取组件的实例对象,进而获取组件上的所有的属性和方法,

// 在react ,如果使用ref,需要先从react中引入 createRef 这个方法
// console.log(createRef);

// 非受控组件
// 定义: 不受state控制,只是初始化的时候使用defaultValue(可选),
// 语法: ref+defaultValue,通过操作dom 原生节点来获取对应的内容

class Child extends Component {
    state = {
        username: '李旺源',
        pwd: 666,
        gender: "男"
    }
    ref1 = createRef();
    ref2 = createRef();
    ref3 = createRef();
    ref4 = createRef();
    ref5 = createRef()
    render() {
        const { username, pwd, gender } = this.state
        return (
            <div>
                <p><input type='text' ref={this.ref1} defaultValue={username} placeholder='用户名' /></p>
                <p><input type='text' ref={this.ref2} defaultValue={pwd} placeholder='密码' /></p>
                <p>
                    <input type='radio' name='gender' ref={this.ref3} value='男' defaultChecked={'男' == gender} />男
                    <input type='radio' name='gender' ref={this.ref4} value='女' defaultChecked={'女' == gender} />女
                </p>
                {/* 使用子组件 */}
                <Grandson ref={this.ref5}></Grandson>
                <p><button onClick={this.submitFn}>提交</button></p>
            </div>
        );
    };
    submitFn = () => {
        // 点击的时候 获取输入的信息
        console.log(this.ref1.current.value);
        console.log(this.ref2.current.value);
        // console.log(this.ref3);
        // console.log(this.ref4);

        if (this.ref3.current.checked) {
            console.log(this.ref3.current.value)
        } else {
            console.log(this.ref4.current.value)
        }

        console.log(this.ref5.current.state.msg)

    }
}

export default Child;
