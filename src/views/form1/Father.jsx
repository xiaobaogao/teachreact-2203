import React, { Component } from 'react';

import Child from './Child';
class Father extends Component {
    render() {
        return (
            <div>
                {/* 使用子组件 */}
                <Child></Child>
            </div>
        );
    }
}

export default Father;
