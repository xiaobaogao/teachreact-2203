// 快捷方式创建函数组件
// 前提需要vscode 安装插件 js jsx Snippets 插件
// 使用 rfc 创建函数组件  react function component

import React from 'react';

const Funchild = (props) => {
    // 该props 就表示父组件传递的属性对象
    console.log(props);
    console.log('Funchild', this);
    const { tit, nameArr } = props
    return (
        <div>
            <p>我是函数子组件--{tit}</p>
            <ul>
                {nameArr.map((item, index) => {
                    return <li key={index}>{item}</li>
                })}
            </ul>
        </div>
    );
}

export default Funchild;


