import React, { Component } from 'react';

// react中的表单使用有2种方式
// 受控组件
// 表单中的value 受state中的数据影响控制
// 结合onChange 可以实现类似vue双向数据绑定

// 非受控组件


class Child extends Component {
    state = {
        username: '牛衍娇',
        pwd: 123456,
        gender: '男'
    }
    render() {
        const { username, pwd, gender } = this.state
        return (
            <div>
                <p>
                    <input type='text' name='username' value={username} onChange={this.edit} />
                </p>
                <p>
                    <input type='text' name='pwd' value={pwd} onChange={this.edit} />
                </p>
                <p>
                    <input type='radio' name='gender' value='男' checked={gender == '男'} onChange={this.edit} /> 男
                    <input type='radio' name='gender' value='女' checked={gender == '女'} onChange={this.edit} /> 女
                </p>
            </div>
        );
    };
    edit = (e) => {
        // console.log(e.target.value);
        // console.log(e.target.name);
        let key = e.target.name
        this.setState({
            [key]: e.target.value
        })

        // 函数形式
        // this.setState((state) => {
        //     state[key] = e.target.value
        //     return state
        // })

    }
}

export default Child;
