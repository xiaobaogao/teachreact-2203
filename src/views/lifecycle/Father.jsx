import React, { Component } from 'react';

// 引入子组件
import Child from './Child'
class Father extends Component {
    state = {
        money: 100
    }
    render() {
        return (
            <div>
                <p>我说父组件--<button onClick={() => { this.setState({ money: this.state.money + 1 }) }}>修改money</button></p>
                {/* 使用子组件 */}
                <Child money={this.state.money}></Child>
            </div>
        );
    }
}

export default Father;
