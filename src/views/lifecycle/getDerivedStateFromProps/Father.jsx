import React, { Component } from 'react';

import Child from './Child'
class Father extends Component {

    state = {
        username: "王国涵",
        age: 20
    }
    render() {
        return (
            <div>
                <p><button onClick={() => { this.setState({ username: '王婉婉' }) }}>修改用户名</button></p>
                <Child userinfo={this.state}></Child>
            </div>
        );
    }
}

export default Father;
