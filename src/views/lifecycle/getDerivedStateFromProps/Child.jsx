import React, { Component } from 'react';

//07: 生命周期函数 getDerivedStateFromProps(props,state)
// 执行时间: 初始执行,更新数据时也触发,
// 作用 使用获取到的props(父组件传过来的) 来更新state(自身组件的state)
// 注意: 该生命周期函数一定要有返回值,return 返回的对象或null ,当返回对象时,他会合并并且覆盖掉自身state中的数据
// 注意: 属性名一致出现覆盖,不一致只是合并
// 参数1: props为父组件传递的参数对象
// 参数2: state为组件自身的state


class Child extends Component {
    state = {
        username1: '马雪艳',
        age1: 19,
        sex: '女'
    }
    render() {
        return (
            <div>
                <p>{this.props.userinfo.username} -- {this.props.userinfo.age}</p>
            </div>
        );
    };
    static getDerivedStateFromProps(props, state) {
        console.log('getDerivedStateFromProps');
        console.log(props, state);
        return { username: props.userinfo.username }
    }

}

export default Child;
