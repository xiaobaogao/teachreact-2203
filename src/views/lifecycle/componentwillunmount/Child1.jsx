import React, { Component } from 'react';

import axios from 'axios'
class Child1 extends Component {
    state = {
        timer: null,
        source: axios.CancelToken.source() // 创建source对象
    }
    render() {
        return (
            <div>
                <p>child1</p>
            </div>
        );
    };
    componentDidMount() {
        console.log('componentDidMount', 'child1');
        // this.timer = setInterval(() => { console.log('setInterval'); }, 1000)
        // this.setState({
        //     timer: this.timer
        // })
        axios.get('https://api.i-lynn.cn/college', {
            cancelToken: this.state.source.token
        }).then(res => {
            console.log(res);
        }).catch(e => { })
    };
    componentWillUnmount() {
        console.log('componentWillUnmount', 'child1');
        // 清除定时器
        // clearInterval(this.state.timer)
        this.state.source.cancel()
    }
}

export default Child1;
