
import React, { Component } from 'react';
import axios from 'axios'
class Child2 extends Component {
    state = {
        source: axios.CancelToken.source() // 创建source对象
    }
    render() {
        return (
            <div>
                <p>child2</p>
            </div>
        );
    };
    componentDidMount() {
        console.log('componentDidMount', 'child2');
        axios.get('https://api.i-lynn.cn/college', {
            cancelToken: this.state.source.token
        }).then(res => {
            console.log(res);
        }).catch(e => { })
    }
    componentWillUnmount() {
        console.log('componentWillUnmount', 'child2');
        this.state.source.cancel()
    }
}

export default Child2;
