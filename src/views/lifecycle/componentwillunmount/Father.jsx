import React, { Component } from 'react';


// 06: 生命周期 componentWillUnmount()
// 作用: 清理副作用,如: 定时器,事件解绑, 取消请求等等类似 卸磨杀驴 


import Child2 from './Child2';
import Child1 from './Child1';
class Father extends Component {
    state = {
        flag: true
    }
    render() {
        const { flag } = this.state
        return (
            <div>
                <p><button onClick={() => { this.setState({ flag: !flag }) }}>切换flag</button></p>
                {this.state.flag ? <Child1></Child1> : <Child2></Child2>}
            </div>
        );
    }
}

export default Father;
