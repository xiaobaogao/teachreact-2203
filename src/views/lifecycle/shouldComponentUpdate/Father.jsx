import React, { Component } from 'react';

//08: 生命周期 shouldComponentUpdate
// 表示是否执行render渲染,在render之前执行,只有更新的时候才触发
// 该生命周期必须有返回值,默认是true, 当为ture 表示渲染该组件,否则不渲染
// 注意: 该方法根据shouldComponentUpdate 返回的boolean 类型值决定该对应的组件是否被渲染,一般作为性能优化的方式

// 注意: 当props传递的参数结构比较简单,不是复杂的数据结构,这时候可以使用使用pureComponent(纯组件),继承纯组件的形式,这样就
// 不用手动去判断了.当结构是复杂的数据结构,建议还是手动比较,因为PureComponent 只会对 props 和 state 进行浅层比较



import Child1 from './Child1';
import Child2 from './Child2';
class Father extends Component {
    state = {
        num1: 0,
        num2: 100
    }
    render() {
        const { num1, num2 } = this.state
        return (
            <div>
                {/* 子组件1 */}
                <Child1 num1={num1}></Child1>
                <p><button onClick={() => { this.setState({ num1: num1 + 1 }) }}>num1+1</button></p>
                <hr />
                {/* 子组件2 */}
                <Child2 num2={num2}></Child2>
                <p><button onClick={() => { this.setState({ num2: num2 + 1 }) }}>num1+1</button></p>
            </div>
        );
    }
}

export default Father;
