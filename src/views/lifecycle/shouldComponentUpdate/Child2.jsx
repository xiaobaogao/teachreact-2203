import React, { Component, PureComponent } from 'react';

class Child2 extends PureComponent {
    render() {
        console.log('render', 'child2');
        return (
            <div>
                <p>child2--{this.props.num2}</p>
            </div>
        );
    };
    // shouldComponentUpdate(nextProps, nextState) {
    //     // console.log('shouldComponentUpdate');
    //     // console.log(nextProps); // 更新后的props
    //     // console.log(this.props); // 更新前的props
    //     if (nextProps.num2 == this.props.num2) {
    //         return false
    //     }

    //     return true
    // }
}

export default Child2;
