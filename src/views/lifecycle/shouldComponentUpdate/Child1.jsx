import React, { Component, PureComponent } from 'react';

class Child1 extends PureComponent {
    render() {
        console.log('render', 'child1');
        return (
            <div>
                <p>child1--{this.props.num1}</p>
            </div>
        );
    };
    // shouldComponentUpdate(nextProps, nextState) {
    //     // console.log('shouldComponentUpdate');
    //     // console.log(nextProps); // 更新后的props
    //     // console.log(this.props); // 更新前的props
    //     if (nextProps.num1 == this.props.num1) {
    //         return false
    //     }
    //     return true
    // }
}

export default Child1;
