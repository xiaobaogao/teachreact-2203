import React, { Component } from 'react';
import axios from 'axios'
// 生命周期函数
// 01: constructor
// 作用: 一般执行初始化操作 如: 初始化state,给事件绑定this 初始化事件执行函数
// 特点: 只有第一次加载时才执行,数据更新操作不会触发constructor函数
// 注意: 不建议在contructor中设置setState,这意味着 不能在该生命周期函数中进行ajax请求
// 根据执行的逻辑顺序,也不建议写在,如果请求报错,那么你连页面都看不见了


// 02:render
// 作用: 用来渲染页面模板的,这个生命周期必需要有
// 特点: 第一次加载时执行,数据更新也执行
// 不能将ajax请求写在render中,因为render一般伴随setState(),而修改state 出触发render,这样就陷入死循环中

//03:componentDidMount() 
//作用: 会在组件挂载后（插入 DOM 树中）立即调用
// 特点: 相当于vue中mounted.此时页面的新的DOM已经更新完毕
// 一般在该生命周期中进行ajax 异步请求

//04: componentDidUpdate(prevProps, prevState, snapshot)
// 作用:当props修改时,或者state 数据修改时触发
// 参数1: prevProps 表示修改前的props 对象
// 参数2: prevState 表示修改前的state 对象
// 参数3: snapshot  

// 05:getSnapshotBeforeUpdate(prevProps, prevState)
// 作用: 在dom 节点更新前然后获取 上一次页面的一些DOM信息(比如:上一次dom 高度, 宽度,滚动条的位置等等)
// 特点: 在数据更新的时候触发,执行事件在render后,componentDidUpdate之前
// 注意: 必须有返回值,返回值 要不snapshot快照DOM信息, 要不就是null


class Child extends Component {
    constructor() {
        super()
        // console.log('constructor');
        // console.log(a);
    }
    state = {
        username: '宋翠',
        age: 16,
        schoolList: [] // 数据请求

    }
    render() {
        //console.log('render');

        return (
            <div className='box'>
                <p>我是子组件--{this.state.username}--{this.state.age}</p>
                {/* 接收的父组件分money */}
                <p>money:{this.props.money}</p>
                <p><button onClick={() => { this.setState({ age: this.state.age + 1 }) }}>修改年龄</button></p>
                <p><button onClick={this.addSchool}>添加学校</button></p>
                <ul>
                    {this.state.schoolList.map((item) => {
                        return <li key={item.id}>{item.school_name}</li>
                    })}
                </ul>
            </div>
        );
    };
    addSchool = () => {
        this.state.schoolList.push({ id: 100, school_name: '千锋教育' })
        this.setState({
            schoolList: this.state.schoolList
        })
    }
    componentDidMount() {
        //console.log('componentDidMount');
        // 进行数据请求
        axios.get('https://api.i-lynn.cn/college').then(res => {
            //console.log(res);
            this.setState({
                schoolList: res.data.data.list1
            })
        })
    };
    getSnapshotBeforeUpdate(prevProps, prevState) {
        console.log('getSnapshotBeforeUpdate');
        // 快照为Dom信息 (上一次页面的一些DOM信息)
        return document.querySelector('.box').clientHeight

    };
    componentDidUpdate(prevProps, prevState, snapshot) {
        // updated
        console.log('componentDidUpdate');
        //参数可选
        // console.log(this.props); // 修改后的props对象
        // console.log(prevProps);// 修改前的props对象
        // console.log(prevState);
        console.log(snapshot);

    };

}

export default Child;

