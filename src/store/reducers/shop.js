import types from "../types"
const defaultSate = {
    num: 100,  // shop商品模块的数据
}

function shop(state = defaultSate, actions) {
    if (actions.type == types.c) {
        return {
            ...state,
            num: state.num + actions.payload
        }
    }

    // 直接返回表示没有修改操作
    return state
}

export default shop
