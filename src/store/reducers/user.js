
// 定义用户模块的数据
import types from "../types"
const defaultSate = {
    count: 0,  // user用户模块的数据
}

function user(state = defaultSate, actions) {
    // 有修改任务使用actions修改state,然后返回修改后的state
    // console.log(actions);
    if (actions.type == types.a) {
        return {
            ...state,
            count: state.count + actions.payload
        }
    }

    if (actions.type == types.b) {
        return {
            ...state,
            count: state.count - actions.payload
        }
    }
    // 直接返回表示没有修改操作
    return state
}
export default user
