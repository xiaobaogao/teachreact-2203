
// 统一管理所有的reducers 中的标识符,这样避免开发者的type 的标识名 起冲突重复
const types = {
    a: '+',
    b: '-',
    c: 'jia'
    // ....
}

export default types