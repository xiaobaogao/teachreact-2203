

// 定义store 实例对象
// 第一步: 引入创建 store实例对象的方法 createStore
import { createStore, combineReducers } from 'redux';

// 第二步: 定义初始state数据
// const defaultSate = {
//     count: 0,  // user用户模块的数据
//     num: 100  // 商品模块的数据
// }

// 第三步: 定义reduce函数 
// 参数1: 表示当前state数据
// 参数2: 表示接收到的修改state数据的任务actions
// 注意: reduce函数必须有返回有返回值,返回值为state 数据,如果你没有修改任务,就返回原state,如果有修改任务,就返回修改后的state
// 注意: actions 为一个对象 格式为{type:'+',可选项参数}
// 注意: 修改store的操作不是修改了store,而是reducer中返回的新的state替换掉了原来的state数据,要想
// 保留原有的其他数据,使用扩展运算符保留
// function reducer(state = defaultSate, actions) {
//     // 有修改任务使用actions修改state,然后返回修改后的state
//     // console.log(actions);
//     if (actions.type == '+') {
//         return {
//             ...state,
//             count: state.count + actions.payload
//         }
//     }

//     if (actions.type == '-') {
//         return {
//             ...state,
//             count: state.count - actions.payload
//         }
//     }

//     if (actions.type == 'jia') {
//         return {
//             ...state,
//             num: state.num + actions.payload
//         }
//     }

//     // 直接返回表示没有修改操作
//     return state
// }

// 导入user模块 shop 模块
// import user from './reducers/user';
// import shop from './reducers/shop';
// // ....

// 由于以上模块的引入方式,如果模块随着业务越来越多,代码量有点冗余,希望有一个方法可以一次性都导入
// 使用webpack 提供的一个方法 require.context(要引入的文件的文件夹路径,是否存在文件递归,规定操作的文件夹下的文件)方法
const files = require.context('./reducers', false, /\.js$/)
// console.log(files.keys());
// 最终的目标 实现 { user, shop }

const reducerObj = {}
files.keys().forEach((item) => {
    // console.log(item);
    let startIndex = item.lastIndexOf('/')
    let lastIndex = item.lastIndexOf('.')
    let key = item.substring(startIndex + 1, lastIndex);
    // console.log(files(item).default);
    reducerObj[key] = files(item).default
})

// console.log(reducerObj);


// const reducers = combineReducers({ user: user, shop: shop })
const reducers = combineReducers(reducerObj)
// 第四步: 创建store实例对象,参数为reducer函数
// 该 store相当于vuex 中创建的store实例对象
const store = createStore(
    reducers,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

// 第五步: 导出 store

export default store











